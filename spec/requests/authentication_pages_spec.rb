require 'spec_helper'

describe "AuthenticationPages" do
	subject { page }

	describe "sign in page" do
		before { visit signin_path }

		describe "with invalid info" do
			before { click_button "Sign in" }

			it { should have_content('Sign in') }
			it { should have_selector('div.alert.alert-error') }

			describe "after failed sign in " do
				before { click_link "Home" }
				it { should_not have_selector('div.alert.alert-error') }
			end
		end

		describe "with valid info" do

			let (:user) { FactoryGirl.create(:user) }
			before do
				fill_in "Email", with: user.email.upcase
				fill_in "Password", with: user.password
				click_button "Sign in"
			end


			it { should have_title(user.name) }
			it { should have_link('Users', href: users_path) }
			it { should have_link('Profile', href: user_path(user)) }
			it { should have_link('Settings', href: edit_user_path(user)) }
			it { should have_link('Sign out', href: signout_path) }
			it { should_not have_link('Sign in', href: signin_path) }
		end
	end

	describe "authorization" do
		describe "for non signed in users" do
			let(:user) { FactoryGirl.create(:user) }


			describe "attempt to edit user" do
				before do
					visit edit_user_path(user) 
					fill_in "Email", with: user.email
					fill_in "Password", with: user.password
					click_button "Sign in"
				end

				describe "after signgin in" do
					it { should have_title('Edit user') }
				end
			end

			describe "visiting the edit page" do
				before { visit edit_user_path(user) }
				it { should have_title('Sign in') }
			end

			describe "submitting to update user" do 
				before { patch user_path(user) }
				specify { expect(response).to redirect_to(signin_path) }
			end

			describe "visiting the users index" do
				before { visit users_path }

				it { should have_title('Sign in') }
			end

			describe "creating a micropost" do
				before { post microposts_path }
				specify { expect(response).to redirect_to(signin_path) }
			end

			describe "deleting micropost" do
				before { delete micropost_path(FactoryGirl.create(:micropost)) }
				specify { expect(response).to redirect_to(signin_path) }
			end
		end

		describe "as a wrong user" do
			let(:user) { FactoryGirl.create(:user) }
			let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
			before { sign_in user, no_capybara: true }

			describe "submit get request" do
				before { get edit_user_path(wrong_user) }
				specify { expect(response.body).not_to match('Ruby on Rails Tutorial Sample App | Edit user') }
				specify { expect(response).to redirect_to(root_url) }
			end

			describe "submitting patch request to users update" do
				before { patch user_path(wrong_user) }
				specify { expect(response).to redirect_to(root_url) }
			end
		end

		describe "as a non admin user" do
			let(:user) { FactoryGirl.create(:user) }
			let(:non_admin) { FactoryGirl.create(:user) }

			before { sign_in non_admin, no_capybara: true }

			describe "submitting delete request to users destory" do
				before { delete user_path(user) }
				specify { expect(response).to redirect_to(root_url) }
			end
		end
	end
end
